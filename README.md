# Correct Citation Omeka Plugin

This plugin correct the French citations using translation string.

## Requirements

Nothing.

## Installation
1. Download the Plugin from its GitLab Repository: [Correct Citation GitLab Repository] and rename the folder to CorrectCitation

[Correct Citation GitLab Repository]: https://gitlab.com/TIME_LAS/Omeka_Plugin_CorrectCitation/

2. Upload the plugin directory to the plugins directory of your
   Omeka installation. See the [Installing a Plugin] page for details.

[Installing a Plugin]: http://omeka.org/codex/Installing_a_Plugin

3. Once it's uploaded and unzipped in the `plugins` directory, go to the plugins management interface by clicking on the "Settings"
   tab at the top right of the administrative interface and selecting
the "Plugins" tab.

4. Click the green "Install" button in the listing for the Switch Language plugin.

